package com.mastertech.usuario.feignclients;

import com.mastertech.usuario.dtos.CepDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cep-microservice", url = "http://localhost:9000/cep-microservice")
public interface CepMicroserviceFeignClient {

    @GetMapping("/cep/{cep}")
    CepDTO getCep(@PathVariable String cep);
}
