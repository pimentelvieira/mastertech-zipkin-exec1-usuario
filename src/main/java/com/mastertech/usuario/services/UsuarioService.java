package com.mastertech.usuario.services;

import com.mastertech.usuario.dtos.CepDTO;
import com.mastertech.usuario.dtos.UsuarioDTO;
import com.mastertech.usuario.feignclients.CepMicroserviceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private CepMicroserviceFeignClient cepMicroserviceFeignClient;

    public UsuarioDTO getUsuario(String nome, String cep) {
        CepDTO cepDTO = this.cepMicroserviceFeignClient.getCep(cep);
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setCep(cepDTO.getCep());
        usuarioDTO.setNome(nome);
        return usuarioDTO;
    }
}
