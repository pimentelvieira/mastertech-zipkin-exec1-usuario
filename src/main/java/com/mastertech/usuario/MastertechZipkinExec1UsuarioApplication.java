package com.mastertech.usuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MastertechZipkinExec1UsuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechZipkinExec1UsuarioApplication.class, args);
	}

}
