package com.mastertech.usuario.controllers;

import com.mastertech.usuario.dtos.UsuarioDTO;
import com.mastertech.usuario.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping("/{nome}/{cep}")
    public UsuarioDTO getUsuario(@PathVariable String nome, @PathVariable String cep) {
        return this.service.getUsuario(nome, cep);
    }
}
